import { Component, OnInit } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { ActivatedRoute } from '@angular/router';
import { DestinosApiClient } from '../models/destinos-api-client.model';
import { Appstate } from '../app.module';

@Component({
  selector: 'app-destino-detalle',
  templateUrl: './destino-detalle.component.html',
  styleUrls: ['./destino-detalle.component.css'],
  providers: [DestinosApiClient]
})

export class DestinoDetalleComponent implements OnInit {
  destino: DestinosApiClient;

  constructor(private route: ActivatedRoute, public destinosApiClient: DestinosApiClient) { }

  ngOnInit(): void {
     const id = this.route.snapshot.paramMap.get('id');
     this.destino = null;
  }

}

import { Injectable, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { Subject, BehaviorSubject } from 'rxjs';
import { Store } from '@ngrx/store';
import { Appstate } from '../app.module';
import { NuevoDestionAction, ElegidoFavoritoAction } from './destinos-viajes-state.model';

@Injectable()
export class DestinosApiClient {
  destinos: DestinoViaje[] = [];

  constructor(private store: Store<Appstate>) {
  }

  add(d: DestinoViaje) {
    this.store.dispatch(new NuevoDestionAction(d));
  }

  elegir(d: DestinoViaje) {
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }

}
